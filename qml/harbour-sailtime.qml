/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.sailtime.UptimeChecker 2.0
import harbour.sailtime.SysinfoC 1.0
import harbour.sailtime.Sysload 1.0
import "cover"
import "pages"
import "js/storage.js" as Storage

ApplicationWindow
{
    id: window

    // Refresh whenever app is activated
    onApplicationActiveChanged: {
        if (applicationActive) {
            updateDisplay();
        }
    }

    FirstPage {
        id: firstpage
    }

    SecondPage {
        id: secondpage
        onChangeTimer: {
            if (enabled) {
                //console.log("Starting timer")
                refreshTimer.start();
            }
            else {
                //console.log("Stopping timer")
                refreshTimer.stop();
            }
        }

        onChangeInterval: {
            //console.log("Timer interval: " + interval)
            refreshTimer.interval = interval * 60 * 1000;
        }

        onChangeRecord: {
            firstpage.setRecordEnabled(enabled);
        }
    }

    CoverPage {
        id: coverpage
    }

    Timer {
        id: refreshTimer
        interval: secondpage.refreshInterval * 60 * 1000
        onTriggered: {
            //console.log("Timer fired!")
            //console.log("Time is now " + new Date());
            updateDisplay()
        }
        repeat: true
        running: secondpage.timerEnable
    }

    function updateDisplay() {
        Storage.db = Storage.connect();

        var sysinfo = checker.fetchUptime();
        var uptime_s = sysinfo.uptime;
        var uptime_d = divideUptime(uptime_s);

        var record = parseInt(Storage.readSetting(Storage.db, "record", 0));
        var record_d = divideUptime(record);

        var uptime = {
            "days": uptime_d.days,
            "hours": uptime_d.hours,
            "minutes": uptime_d.minutes,
            "load1": sysinfo.loads.avg_1,
            "load5": sysinfo.loads.avg_5,
            "load15": sysinfo.loads.avg_15,
            "record": record,
            "rDays": record_d.days,
            "rHours": record_d.hours,
            "rMinutes": record_d.minutes,
        }

        firstpage.updatePage(sysinfo, uptime);
        coverpage.updateCover(uptime);

        var recordEnable = Storage.readSetting(Storage.db, "recordEnable", false);

        // Update new record to db if feature is activated
        if (recordEnable && (uptime_s >= record)) {
            Storage.storeSetting(Storage.db, "record", uptime_s);
        }
    }

    // Divide uptime to days, hours and minutes
    function divideUptime(seconds) {
        var days = Math.floor(seconds / 86400);
        var hours = Math.floor(seconds / 3600 % 24);
        var minutes = Math.floor(seconds / 60 % 60);
        return {
            "days": days,
            "hours": hours,
            "minutes": minutes
        };
    }

    // Return formatted language strings with plural forms
    function formatUptime(days, hours, minutes) {
        var days_str = days + " day" + ((days !== 1)? "s" : "");
        var hours_str = hours + " h";
        var minutes_str = minutes + " min";
        return {
            "days": days_str,
            "hours": hours_str,
            "minutes": minutes_str
        };
    }

    UptimeChecker {
        id: checker
    }

    initialPage: firstpage
    cover: coverpage
}
