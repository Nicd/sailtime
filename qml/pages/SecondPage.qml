/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../js/storage.js" as Storage


Dialog {
    id: dialog

    signal changeTimer(bool enabled)
    signal changeInterval(int interval)
    signal changeRecord(bool enabled)

    property alias timerEnable : timerEnable
    property alias refreshInterval : intervalSlider

    function getSettings() {
        Storage.db = Storage.connect();
        var setVal = Storage.readSetting(Storage.db, "interval", 15.0);
        if (setVal !== undefined) {
            intervalSlider.value = setVal;
        }
        else {
            intervalSlider.value = 1.0;
        }

        timerEnable.checked = Storage.readSetting(Storage.db,
                                                  "timerEnable", false);

        recordEnable.checked = Storage.readSetting(Storage.db,
                                                   "recordEnable", false);
    }

    // Load values when app is started to start timers if needed
    Component.onCompleted: {
        getSettings();
        changeTimer(timerEnable.checked);
        changeInterval(intervalSlider.value);
        changeRecord(recordEnable.checked);
    }

    onOpened: {
        getSettings();
    }

    onAccepted: {
        Storage.storeSetting(Storage.db,
                             "timerEnable",
                             timerEnable.checked);
        changeTimer(timerEnable.checked);

        Storage.storeSetting(Storage.db,
                             "interval",
                             intervalSlider.value);
        changeInterval(intervalSlider.value);

        Storage.storeSetting(Storage.db,
                             "recordEnable",
                             recordEnable.checked);
        changeRecord(recordEnable.checked);
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: dialog.width
            spacing: Theme.paddingLarge

            DialogHeader {
                title: "Settings"
                acceptText: "Save"
                cancelText: "Cancel"
            }

            TextSwitch {
                id: timerEnable
                checked: false
                text: "Automatically refresh cover"
            }

            Slider {
                id: intervalSlider
                anchors.horizontalCenter: parent.horizontalCenter
                label: "Refresh interval"
                minimumValue: 1
                maximumValue: 60
                stepSize: 1
                valueText: value + " min"
                width: parent.width - Theme.paddingLarge * 2
            }

            Label {
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.secondaryColor
                text: "Note that due to the power saving features of Sailfish OS, the update may not occur at the specified interval when the device is locked."
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - Theme.paddingLarge * 2
                wrapMode: Text.WordWrap
            }

            TextSwitch {
                id: recordEnable
                checked: false
                text: "Store uptime record"
            }

            Label {
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.secondaryColor
                text: "Record is stored whenever the uptime is updated."
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - Theme.paddingLarge * 2
                wrapMode: Text.WordWrap
            }
        }
    }
}





