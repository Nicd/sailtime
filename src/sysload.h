#ifndef SYSLOAD_H
#define SYSLOAD_H

#include <QObject>
#include <sys/sysinfo.h>

class Sysload : public QObject
{
    Q_OBJECT
public:
    explicit Sysload(float loads[3], QObject* parent = 0);

    Q_PROPERTY(float avg_1 READ getAvg1)
    Q_PROPERTY(float avg_5 READ getAvg5)
    Q_PROPERTY(float avg_15 READ getAvg15)

    float getAvg1() const;
    float getAvg5() const;
    float getAvg15() const;

signals:

public slots:

private:
    float avg_1;
    float avg_5;
    float avg_15;


};

#endif // SYSLOAD_H
